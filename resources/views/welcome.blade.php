<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
   
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

<form action="/create" method="POST">
@csrf
  <div class="form-group row">
    <label for="nombre" class="col-sm-1 col-form-label">Nombre</label>
    <div class="col-sm-10">
      <input type="text" name="nombre" class="form-control" id="nombre" placeholder="name">
    </div>
  </div>
  
  <div class="form-group row">
    <label for="apellido" class="col-sm-1 col-form-label">Apellido</label>
    <div class="col-sm-10">
      <input type="text"  name="apellido" class="form-control" id="apellido" placeholder="Surname">
    </div>
  </div>


  <div class="form-group row">
    <div class="col-sm-10">
      <button type="submit" class="btn btn-primary">Sign in</button>
    </div>
  </div>
  
</form>
</body>
</html>
